# BeautySchedule  
| Aluna| Matrícula |
| ------ | ------ |
| Gabriela Medeiros da Silva | 16/0121817 |
| Geovana Ramos Sousa Silva | 16/0122180|
### Introdução
BeautySchedule é um aplicativo web que tem como finalidade facilitar a interação entre uma pessoa que oferece determinados serviços, nesse caso um salão de beleza, e outra que deseja contratá-los, proporcionando um sistema de marcação de horários.  
O usuário que se cadastra no web app terá acesso aos horários disponíveis no salão, que são cadastrados pelo usuário administrador, e poderá marcar o horário que gostaria de ser atendido e o serviço desejado. Ao fazer isso, o horário é associado a sua conta pessoal e é excluido da lista de horários disponíveis.  
Caso ele opte por desmarcar horário, o mesmo volta para a lista de horários disponíveis.
O administrador tem acesso aos horários de cada usuário e pode ver o nome de cada um, além de poder excluir ou adicionar: Horários disponíveis, horários marcados e usuários.   

### Como rodar esta aplicação
Baixe ou clone esta pasta
```sh
$ git clone https://gitlab.com/GeovanaRamos/EP3.git
```
Instale as dependências
```sh
$ bundle install
```
Inicie o banco de dados
```sh
$ rake db:create
```
Carregue o banco de dados a partir do schema.rb 
    
```sh
$ rake db:schema:load
```
Rode o servidor local
```sh
$ rails s
```
Acesse o site no browser
```sh
http://localhost:3000/
```
Cadastre UM usuário no site para torna-lo administrador. Então vá ao terminal e digite os seguintes comandos.
```sh
$ rails c
>  u = User.first
>  u.admin = true
```
Agora cadastre quantos usuários desejar e faça uso do site. Lembrando que o primeiro usuário tem permissão de administrador.
### Versões das plataformas
* **Versões**
     * _Ruby_: _2.4.2 (x86_64-linux)_
     * _Rails_: 5.1.4

### Requisitos cumpridos
*  **Projeto de Aplicativo Web em _Ruby on Rails_**

     O aplicativo foi formado a partir da linguagem Ruby on Rails.

* **Classes na _model_**
 
     Há 3 classes presentes na model. São elas:
     * Myschedule
     * Schedule
     * User
    
* **Acesso de usuários cadastrados**
 
     Só é possível marcar um horário através do site se o usuário for cadastrado e estiver usando sua conta.

* **Layout mais elaborado**
 
     Foi utilizada a linguagem CSS (Cascading Style Sheets ) e HTML para a formatação e edição de de todas as páginas existentes no site. Foi feito o uso de um template básico, mas a formatação em geral foi feita a mão.

* **Relação entre as models**
 
     Há duas relações entre as classes anteriormente citadas:
     _User has_many myschedules_ e
     _myschedyle belong_to users_

* **Gerência de níveis de permissão por tipo de usuário**
 
     Há dois tipos de usuário que têm acesso às ferramentas oferecidas pelo aplicativo:
     * O cliente, que é quem contrata os serviços disponíveis de acordo com horários e dias já cadastrados pelo administrador e
     * O administrador, que tem permissão para adicionar os horários que devem estar disponíveis para o cliente, assim como para editá-los ou excluí-los. 

* **Uso de outras gems**
     * **gem 'devise', '~> 4.3.0'** Autenticação de usuários
     * **gem 'rails_admin', '>= 1.0.0.rc'** Página de administrador 
     * **gem "table_print"** Visualizar tabelas sql no console

### Links
[Vídeo demonstrativo](https://www.youtube.com/watch?v=AB0mD6I0a14)      

[Relatório](https://gitlab.com/GeovanaRamos/EP3/blob/master/doc/Relat%C3%B3rio%20da%20aplica%C3%A7%C3%A3o.pdf)

