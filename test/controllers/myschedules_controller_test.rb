require 'test_helper'

class MyschedulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @myschedule = myschedules(:one)
  end

  test "should get index" do
    get myschedules_url
    assert_response :success
  end

  test "should get new" do
    get new_myschedule_url
    assert_response :success
  end

  test "should create myschedule" do
    assert_difference('Myschedule.count') do
      post myschedules_url, params: { myschedule: { day: @myschedule.day, month: @myschedule.month, schedule_id: @myschedule.schedule_id, user_id: @myschedule.user_id, year: @myschedule.year } }
    end

    assert_redirected_to myschedule_url(Myschedule.last)
  end

  test "should show myschedule" do
    get myschedule_url(@myschedule)
    assert_response :success
  end

  test "should get edit" do
    get edit_myschedule_url(@myschedule)
    assert_response :success
  end

  test "should update myschedule" do
    patch myschedule_url(@myschedule), params: { myschedule: { day: @myschedule.day, month: @myschedule.month, schedule_id: @myschedule.schedule_id, user_id: @myschedule.user_id, year: @myschedule.year } }
    assert_redirected_to myschedule_url(@myschedule)
  end

  test "should destroy myschedule" do
    assert_difference('Myschedule.count', -1) do
      delete myschedule_url(@myschedule)
    end

    assert_redirected_to myschedules_url
  end
end
