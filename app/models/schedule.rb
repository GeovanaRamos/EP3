class Schedule < ApplicationRecord

  def day_enum
    [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
    21,22,23,24,25,26,27,28,29,30,31]
  end
  def month_enum
    ['Janeiro','Fevereiro','Março', 'Abril', 'Maio', 'Junho',
    'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
  end
  def year_enum
    [2017,2018,2019,2020]
  end
  def service_enum
    ['Corte','Hidratação','Escova','Cauterização', 'Progressiva']
  end


end
