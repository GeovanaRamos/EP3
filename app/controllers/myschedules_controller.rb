class MyschedulesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_myschedule, only: [:show, :edit, :update, :destroy]
  $schedule_id = nil
  # GET /myschedules
  # GET /myschedules.json
  def index
    @user = current_user
    @myschedules = @user.myschedules.all
  end

  # GET /myschedules/1
  # GET /myschedules/1.json
  def show
  end

  # GET /myschedules/new
  def new
    @myschedule = Myschedule.new
    @time = params[:time]
    @day = params[:day]
    @month = params[:month]
    @year = params[:year]
    $schedule_id = params[:schedule_id]
    @service = params[:service]
  end

  # GET /myschedules/1/edit
  def edit
  end

  # POST /myschedules
  # POST /myschedules.json
  def create
    @user = current_user
    @myschedule = @user.myschedules.build(myschedule_params)

    respond_to do |format|
      if @myschedule.save
        Schedule.find($schedule_id).destroy
        format.html { redirect_to @myschedule, notice: 'Seu horário foi marcado' }
        format.json { render :show, status: :created, location: @myschedule }
      else
        format.html { render :new }
        format.json { render json: @myschedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /myschedules/1
  # PATCH/PUT /myschedules/1.json
  def update
    respond_to do |format|
      if @myschedule.update(myschedule_params)
        format.html { redirect_to @myschedule, notice: 'Myschedule was successfully updated.' }
        format.json { render :show, status: :ok, location: @myschedule }
      else
        format.html { render :edit }
        format.json { render json: @myschedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /myschedules/1
  # DELETE /myschedules/1.json
  def destroy
    @schedule = Schedule.new(time: @myschedule.time, day: @myschedule.day, month: @myschedule.month, year: @myschedule.year, service: @myschedule.service)
    @schedule.save
    @myschedule.destroy
    respond_to do |format|
      format.html { redirect_to myschedules_url, notice: 'Seu horário foi desmarcado' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_myschedule
      @myschedule = Myschedule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def myschedule_params
      params.require(:myschedule).permit(:time, :day, :month, :year, :user_id, :service)
    end
end
