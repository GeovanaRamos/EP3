json.extract! schedule, :id, :day, :month, :year, :created_at, :updated_at
json.url schedule_url(schedule, format: :json)
