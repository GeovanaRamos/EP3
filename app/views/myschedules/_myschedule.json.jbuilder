json.extract! myschedule, :id, :day, :month, :year, :user_id, :schedule_id, :created_at, :updated_at
json.url myschedule_url(myschedule, format: :json)
